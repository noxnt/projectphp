<!DOCTYPE html>
<html lang="ru">
  <head>
    <?php include('elems/head.php'); ?>
    <title><?= $title ?></title>
  </head>

  <body>
    <header>
      <?php include('elems/header.php'); ?>
    </header>
    <main>
      <?php if ($currentPage == 'articles') include('elems/articles.php'); else echo $content; ?>
    </main>
    <footer>
      <?php include('elems/footer.php'); ?>
    </footer>
  </body>
</html>