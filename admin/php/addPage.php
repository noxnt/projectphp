<?php
  include ('../../elems/init.php');

  if (!empty($_SESSION['auth']) && $_SESSION['auth'] == true) {
    // Для формы
    $title =   (isset($_GET['title']))   ? $_GET['title']   : '';
    $url =     (isset($_GET['url']))     ? $_GET['url']     : '';
    $name =    (isset($_GET['name']))    ? $_GET['name']    : '';
    $content = (isset($_GET['content'])) ? $_GET['content'] : '';

    
    // Есть запрос на добавление страницы
    if (isset($_GET['addPageSubmit'])) {
      // Запрос корректный
      if (!empty($_GET['title']) && !empty($_GET['url']) && !empty($_GET['name']) && !empty($_GET['content'])) {
        // Уникальность url в БД
        $result = mysqli_query($link, "SELECT url FROM pages WHERE url='$_GET[url]'");
        $result = mysqli_fetch_assoc($result);
        $unique = ($result == NULL) ? true : false; 


        if ($unique == true) { // url уникальный
          $title = mysqli_real_escape_string($link, $_GET['title']);
          $url = mysqli_real_escape_string($link, $_GET['url']);
          $name = mysqli_real_escape_string($link, $_GET['name']);
          $content = mysqli_real_escape_string($link, $_GET['content']);

          $query = "INSERT INTO pages (title, url, name, content) VALUES ('$title', '$url', '$name', '$content')";
          mysqli_query($link, $query);

          $_SESSION['info'] = ['message' => 'Страница была добавлена!', 'status' => 'success']; // info

        } else { // url не уникальный
          $_SESSION['info'] = ['message' => "Страница с таким url ($_GET[url]) уже существует!", 'status' => 'error']; // info
        }

      // Запрос не корректный
      } else {
        $_SESSION['info'] = ['message' => 'Все поля должны быть заполнены!', 'status' => 'error']; // info
      }
      header("Location: addPage.php?title=$title&url=$url&name=$name&content=$content"); die();
    }


    // Форма создания страницы
    $createPageForm = '
        <form method="GET">
          Введите title: <input name="title" value="'.$title.'">
          Введите url: <input name="url" value="'.$url.'">
          Введите name: <input name="name" value="'.$name.'">
          Введите content: <textarea name="content">'.$content.'</textarea>
          <input name="addPageSubmit" type="submit">
        </form>
        <p><a class="button" href="../index.php">На главную</a>';


    // Макет страницы
    $thisCreatePage = true; // Это страница "добавления страниц"
    $title = " - add page";
    $toCss = "../css/style.css";
    include ('../layout.php');
  } else {
    header('Location: php/auth.php');
  }