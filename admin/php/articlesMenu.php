<?php
  include ('../../elems/init.php');

  if (!empty($_SESSION['auth']) && $_SESSION['auth'] == true) {
    // Удаление статей
    include('includes/deleteArticle.php');


    // Получение названий и дат всех статей
    $query = "SELECT id, name, date FROM articles ORDER BY date DESC";
    $result = mysqli_query($link, $query);
    for ($articles = []; $row = mysqli_fetch_assoc($result); $articles[] = $row);


    // Кол-во всех статей
    $count = count($articles);


    // Функция для создания таблицы
    if ($count > 0) {
      include('includes/tableCreate.php');
      $table = tableCreate($articles, 'editArticle.php?editArticle', 'deleteArticle', 'id');
    }


    $thisArticlesPage = true; // Это страница "меню статей"
    $title = " - articles menu";
    $toCss = "../css/style.css";
    include ('../layout.php');
  } else {
    header('Location: php/auth.php');
  }