<?php
  if (isset($_GET['deletePage'])) {
    $url = mysqli_real_escape_string($link, $_GET['deletePage']);

    if ($url == 'index' || $url == '404' || $url == 'articles') {
      $_SESSION['info'] = ['message' => "Вы не можете удалить эту страницу!", 'status' => 'error']; // info

    } else {
      // Проверка, существует ли страница в БД
      $query = trim("SELECT url FROM pages WHERE url='$url'");
      $result = mysqli_query($link, $query);
      $exist = mysqli_fetch_assoc($result);

      // Страница не существует
      if (!$exist) {
        $_SESSION['info'] = ['message' => "Страница не существует (url '$url')!", 'status' => 'error']; // info
      } else {
        // Удаление страницы
        $query = "DELETE FROM pages WHERE url='$url'";
        mysqli_query($link, $query);
        $_SESSION['info'] = ['message' => "Страница успешно удалена (url '$url')!", 'status' => 'success']; // info
      }
    }
    header('Location: index.php'); die();
  }