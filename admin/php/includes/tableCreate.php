<?php
  function tableCreate($array, $href1 = 'php/editPage.php?editPage', $href2 = 'deletePage', $by = 'url') {
    $count = count($array); // Кол-во строк (записей) в таблице
    $countTh = count($array[0]); // Кол-во столбцов (параметров) в таблице

    // Начало таблицы
    $table = "<table>
                <tr>";

    // Оглавление столбцов
    foreach ($array[0] as $key => $elem) {
      $table .= "<th>$key</th>";
    }

    // Конец оглавления столбцов
    $table .= "<th>Edit</th>
               <th>Delete</th>
             </tr>";

    // Строки
    foreach ($array as $elem) {
      $table .= "<tr>";
      foreach ($elem as $subKey => $subElem) {
        $table .= "<td>$subElem</td>";
      }

      // Кнопка редактирования
      $table .= "<td><a href=\"$href1=$elem[$by]\">Редактировать</a></td>
                 <td><a href=\"?$href2=$elem[$by]\">Удалить</a></td>
               <tr>";
    }

    // Конец таблицы
    $table .= "</tr>
             </table><hr>";
    return $table;
  }