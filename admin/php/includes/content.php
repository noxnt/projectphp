<?php
  // Формирование содержимого страниц
  $content = '';

  // Таблица с страницами
  $content .= (isset($table)) ? $table : '';

  // Форма создания страниц
  $content .= (isset($thisCreatePage)) ? $createPageForm : '';

  // Форма создания статей
  $content .= (isset($thisAddArticlePage)) ? $createArticleForm : '';

  // Форма редактирования страниц
  $content .= (isset($thisEditPage)) ? $editPageForm : '';

  // Кнопки на главной
  if (isset($thisIndexPage)) {
    $content .= '<a class="button" href="php/articlesMenu.php">Статьи</a>'; // Редактирование статей
    $content .= '<a class="button" href="php/addPage.php">Добавить страницу</a>'; // Добавление страниц
    $content .= '<a class="button" href="php/logout.php">Выход</a>'; // Выход
  }

  // Кнопки в меню статей
  if (isset($thisArticlesPage)) {
    $content .= '<a class="button" href="../index.php">Главная</a>'; // Редактирование статей
    $content .= '<a class="button" href="addArticle.php">Добавить статью</a>'; // Добавление страниц
  }
  

  echo $content;