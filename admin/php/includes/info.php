<?php
  if (isset($_SESSION['info'])) {
    $message = $_SESSION['info']['message'];
    $class = $_SESSION['info']['status'];
    echo "<p class=\"$class\">$message</p>";

    unset($_SESSION['info']);
  }