<?php
  if (isset($_GET['deleteArticle'])) {
    $id = mysqli_real_escape_string($link, $_GET['deleteArticle']);

    // Проверка, существует ли статья в БД
    $query = trim("SELECT id FROM articles WHERE id='$id'");
    $result = mysqli_query($link, $query);
    $exist = mysqli_fetch_assoc($result);

    // Статья не существует
    if (!$exist) {
      $_SESSION['info'] = ['message' => "Статья не существует (id '$id')", 'status' => 'error']; // info
    } else {
      // Удаление страницы
      $query = "DELETE FROM articles WHERE id='$id'";
      mysqli_query($link, $query);
      $_SESSION['info'] = ['message' => "Статья успешно удалена (id '$id')", 'status' => 'success']; // info
    }

    header('Location: articlesMenu.php'); die();
  }