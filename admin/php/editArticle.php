<?php
  include ('../../elems/init.php');

  if (!empty($_SESSION['auth']) && $_SESSION['auth'] == true) {
    // Получение массива редактируемых данных по запросу
    if (isset($_GET['editArticle'])) {
      $query = "SELECT * FROM articles WHERE id='$_GET[editArticle]'";
      $result = mysqli_query($link, $query);

      $article = mysqli_fetch_assoc($result);
      $_SESSION['edit']['currentArticleName'] = $article['name'];
      $_SESSION['edit']['currentId'] = $_GET['editArticle'];
    }


    // Форма отправлена
    if (isset($_GET['editArticleSubmit'])) {
      if (!empty($_GET['name']) && !empty($_GET['text']) && !empty($_GET['date'])) {

        // Уникальность названия в БД
        if ($_SESSION['edit']['currentArticleName'] == $_GET['name']) // название не изменяется
          $unique = true;
        else {
          $result = mysqli_query($link, "SELECT name FROM articles WHERE name='$_GET[name]'");
          $result = mysqli_fetch_assoc($result);
          $unique = ($result == NULL) ? true : false; // название изменяется и такого нету / есть
        }

        // Уникальное название
        if ($unique == true) {
          $name = mysqli_real_escape_string($link, $_GET['name']);
          $text = mysqli_real_escape_string($link, $_GET['text']);
          $date = mysqli_real_escape_string($link, $_GET['date']);

          $query = "UPDATE articles SET name='$name', text='$text', date='$date' WHERE id='{$_SESSION['edit']['currentId']}'";
          mysqli_query($link, $query);

          $_SESSION['info'] = ['message' => 'Статья успешно изменена!', 'status' => 'success']; // info
          header('Location: articlesMenu.php'); die(); // успешно

        // Не уникальное название
        } else {
          $_SESSION['info'] = ['message' => "Статья с таким названием уже существует ($_GET[name])!", 'status' => 'error']; // info
          header("Location: ?editArticle={$_SESSION['edit']['currentId']}"); die();
        }
      } else {
        $_SESSION['info'] = ['message' => 'Все поля должны быть заполнены!', 'status' => 'error']; // info
        header("Location: ?editArticle={$_SESSION['edit']['currentId']}"); die();
      }
    } 


    // Форма редактирования страницы
    $editPageForm = '
      <form method="GET">
        Введите name: <input name="name" value="'.$article['name'].'">
        Введите date: <input name="date" value="'.$article['date'].'">
        Введите text: <textarea name="text">'.$article['text'].'</textarea>
        <input name="editArticleSubmit" type="submit">
      </form>
      <p><a class="button" href="../index.php">На главную</a>';


    // Макет страницы
    $thisEditPage = true; // Это страница "редактирования страниц"
    $title = " - edit articles";
    $toCss = "../css/style.css";
    include ('../layout.php');
  } else {
    header('Location: php/auth.php');
  }