<?php
  include ('../../elems/init.php');

  if (!empty($_SESSION['auth']) && $_SESSION['auth'] == true) {
    // Получение массива редактируемых данных по запросу
    if (isset($_GET['editPage'])) {
      $url = mysqli_real_escape_string($link, $_GET['editPage']);
      $query = "SELECT * FROM pages WHERE url='$url'";
      $result = mysqli_query($link, $query);

      $page = mysqli_fetch_assoc($result);
      $_SESSION['edit']['currentUrl'] = $url; // первоначальный url
    }


    // Форма отправлена
    if (isset($_GET['editPageSubmit'])) {
      if (!empty($_GET['title']) && !empty($_GET['url']) && !empty($_GET['name']) && isset($_GET['content'])) {

        // Уникальность url в БД
        if ($_SESSION['edit']['currentUrl'] == $_GET['url']) // url не изменяется
          $unique = true;
        else {
          $result = mysqli_query($link, "SELECT url FROM pages WHERE url='$_GET[url]'");
          $result = mysqli_fetch_assoc($result);
          $unique = ($result == NULL) ? true : false; // url изменяется и такого нету / есть
        }

        // Уникальный
        if ($unique == true) {
          $curUrl = $_SESSION['edit']['currentUrl']; // первоначальный url

          $title = mysqli_real_escape_string($link, $_GET['title']);
          $url = ($curUrl == 'index' || $curUrl == 'articles') ? $curUrl : $_GET['url']; // index и articles не меняются
          $url = mysqli_real_escape_string($link, $url);
          $name = mysqli_real_escape_string($link, $_GET['name']);
          $content = ($_SESSION['edit']['currentUrl'] == 'articles') ? '' : mysqli_real_escape_string($link, $_GET['content']); // articles остаётся пустым

          $query = "UPDATE pages SET title='$title', url='$url', name='$name', content='$content' WHERE url='{$_SESSION['edit']['currentUrl']}'";
          mysqli_query($link, $query);

          $_SESSION['info'] = ['message' => 'Страница успешно изменена!', 'status' => 'success']; // info
          header('Location: ../index.php'); die(); // успешно

        // Не уникальный
        } else {
          $_SESSION['info'] = ['message' => "Страница с таким url ($_GET[url]) уже существует!", 'status' => 'error']; // info
          header("Location: ?editPage={$_SESSION['edit']['currentUrl']}"); die();
        }
      } else {
        $_SESSION['info'] = ['message' => 'Все поля должны быть заполнены!', 'status' => 'error']; // info
        header("Location: ?editPage={$_SESSION['edit']['currentUrl']}"); die();
      }
    } 


    // Форма редактирования страницы
    $editPageForm = '
      <form method="GET">
        Введите title: <input name="title" value="'.$page['title'].'">
        Введите url: <input name="url" value="'.$page['url'].'">
        Введите name: <input name="name" value="'.$page['name'].'">
        Введите content: <textarea name="content">'.$page['content'].'</textarea>
        <input name="editPageSubmit" type="submit">
      </form>
      <p><a class="button" href="../index.php">На главную</a>';


    // Макет страницы
    $thisEditPage = true; // Это страница "редактирования страниц"
    $title = " - edit page";
    $toCss = "../css/style.css";
    include ('../layout.php');
  } else {
    header('Location: php/auth.php');
  }