<?php
  include('../../elems/init.php');
  include('includes/authData.php');
  if (isset($_POST['submit']) && md5($_POST['login']) == $login && md5($_POST['password']) == $password) {
    $_SESSION['auth'] = true;
    $_SESSION['info'] = ['message' => 'Успешная авторизация', 'status' => 'success'];
    header('Location: ../index.php'); die();
  } else {
    include('includes/authForm.php');
  }

  if (isset($_POST['submit']) && !empty($_POST['login']) && !empty($_POST['password']))
    echo '<p style="color: red;">Неверный логин или пароль!</p>';