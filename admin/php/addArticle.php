<?php
  include ('../../elems/init.php');

  if (!empty($_SESSION['auth']) && $_SESSION['auth'] == true) {
    // Для формы
    $name = (isset($_GET['name'])) ? $_GET['name'] : '';
    $text = (isset($_GET['text'])) ? $_GET['text'] : '';

    
    // Есть запрос на добавление статьи
    if (isset($_GET['addArticleSubmit'])) {
      // Запрос корректный
      if (!empty($_GET['name']) && !empty($_GET['text'])) {
        // Уникальность названия в БД
        $result = mysqli_query($link, "SELECT name FROM articles WHERE name='$_GET[name]'");
        $result = mysqli_fetch_assoc($result);
        $unique = ($result == NULL) ? true : false;


        if ($unique == true) { // название уникальное
          $name = mysqli_real_escape_string($link, $_GET['name']);
          $text = mysqli_real_escape_string($link, $_GET['text']);
          $date = date('Y-m-d H:i:s');

          $query = "INSERT INTO articles (name, text, date) VALUES ('$name', '$text', '$date')";
          mysqli_query($link, $query);

          $_SESSION['info'] = ['message' => 'Статья была добавлена!', 'status' => 'success']; // info

        } else { // url не уникальный
          $_SESSION['info'] = ['message' => "Статья с таким названием уже существует ($_GET[name])!", 'status' => 'error']; // info
        }

      // Запрос не корректный
      } else {
        $_SESSION['info'] = ['message' => 'Все поля должны быть заполнены!', 'status' => 'error']; // info
      }
      header("Location: addArticle.php?name=$name&text=$text"); die();
    }


    // Форма создания страницы
    $createArticleForm = '
        <form method="GET">
          Enter name: <input name="name" value="'.$name.'">
          Enter text: <textarea name="text">'.$text.'</textarea>
          <input name="addArticleSubmit" type="submit">
        </form>
        <p><a class="button" href="articlesMenu.php">Назад</a>';


    // Макет страницы
    $thisAddArticlePage = true; // Это страница "добавления страниц"
    $title = " - add article";
    $toCss = "../css/style.css";
    include ('../layout.php');
  } else {
    header('Location: php/auth.php');
  }