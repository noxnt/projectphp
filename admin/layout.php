<?php
  if (!empty($_SESSION['auth']) && $_SESSION['auth'] == true) {
?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<?= $toCss ?>">
    <title>Admin<?= $title ?></title>
  </head>

  <body>
    <header>
      <h2>Header | Admin page</h2>
    </header>

    <main>
      <?php
        // Содержимое
        include ('php/includes/content.php');

        // Инфо
        include('php/includes/info.php');
      ?>
    </main>

    <footer>
      <h2>Footer</h2>
    </footer>
  </body>
</html>
<?php 
  } else {
    header('Location: php/auth.php');
  }