<?php
  include ('../elems/init.php');


  if (!empty($_SESSION['auth']) && $_SESSION['auth'] == true) {
    // Удаление страниц
    include('php/includes/deletePage.php');
    

    // Получение страницы с БД
    $query = "SELECT id, title, url, name FROM pages"; // Всё кроме контента
    $result = mysqli_query($link, $query);
    for ($pages = []; $row = mysqli_fetch_assoc($result); $pages[] = $row);


    // Функция для создания таблицы
    include('php/includes/tableCreate.php');
    $table = tableCreate($pages);


    // Макет страницы
    $thisIndexPage = true; // Это главная страница
    $title = ' - main';
    $toCss = "css/style.css";
    include ('layout.php');
  } else {
    header('Location: php/auth.php');
  }