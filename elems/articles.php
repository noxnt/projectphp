<?php
  // Получение названий и дат всех статей
  $query = "SELECT id, name, date FROM articles ORDER BY date DESC";
  $result = mysqli_query($link, $query);
  for ($articles = []; $row = mysqli_fetch_assoc($result); $articles[] = $row);


  // Кол-во всех статей
  $count = count($articles);
  

  // Текущая статья
  $id = (!empty($_GET['article'])) ? $_GET['article'] : $articles[0]['id'];
  $query = "SELECT name, text, date FROM articles WHERE id=$id";
  $result = mysqli_query($link, $query);
  $article = mysqli_fetch_assoc($result);


  // Статья
  $articleName = (!empty($article['name'])) ? $article['name'] : 'Статья не найдена' ;
  $articleText = (!empty($article['text'])) ? $article['text'] : '<a href="index.php">Вернуться в начало</a>' ;
  $articleDate = (!empty($article['date'])) ? $article['date'] : '' ;
?>
<div class="articles">
  <nav class="articles__nav">
    <h2>Статьи:</h2><hr>
    <ul>
      <?php
        // Создание элементов навигации
        for ($i = 0; $i < $count; $i++) {
          $navId = $articles[$i]['id'];
          $name = $articles[$i]['name'];
          $date = $articles[$i]['date'];

          $class = ($navId == $id) ? ' class="active"' : '';

          echo "<li><a href=\"/articles/$navId\"$class>$name</a><span>$date</span><hr></li>";
        }
      ?>
    </ul>
  </nav>
  <div class="articles__article">
    <h1><?= $articleName ?></h1>
    <p class="articles__text"><?= $articleText ?></p>
    <p class="articles__date"><?= $articleDate ?></p>
  </div>
</div>