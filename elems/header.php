<?php
  // Получение url и названий всех страниц
    $query = "SELECT url, name FROM pages";
    $result = mysqli_query($link, $query);
    for ($names = []; $row = mysqli_fetch_assoc($result); $names[] = $row);


    // Кол-во всех страниц
    $count = count($names);

?>
<nav>
  <ul>
    <?php
      // Создание элементов навигации
      for ($i = 0; $i < $count; $i++) {
        $url = $names[$i]['url'];
        $name = $names[$i]['name'];

        $class = '';
        if ($url == $currentPage) $class = ' class="active"'; // Текущая страница

        echo "<li><a href=\"/$url/\"$class>$name</a></li>"; // Элементы навигации
      }
    ?>
  </ul>
</nav>