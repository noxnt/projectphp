<?php
  include ('elems/init.php'); // Подключение к БД


  if (!empty($_GET['page'])) {
    $currentPage = $_GET['page'];
  } else {
    $currentPage = 'index';
  }


  // Получение страницы с БД
  $query = "SELECT * FROM pages WHERE url='$currentPage'";
  $result = mysqli_query($link, $query);
  $page = mysqli_fetch_assoc($result);


  // 404
  if (!$page) {
    $query = "SELECT * FROM elems WHERE url='404'";
    $result = mysqli_query($link, $query);
    $page = mysqli_fetch_assoc($result);
    header ("HTTP/1.0 404 Not Found");
  }


  $title = $page['title'];
  $content = $page['content'];

  // Макет страницы
  include ('layout.php'); 